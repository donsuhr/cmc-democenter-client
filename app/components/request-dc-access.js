import config from 'config';

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

function reRequestDcAccess(token) {
    const endpoint = `${config.domains.api}/users/request-access`;
    return fetch(endpoint, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`,
        },
    })
        .then(checkStatus)
        .then(parseJSON)
        .catch((error) => {
            // eslint-disable-next-line no-console
            console.log('request failed', error);
            throw error;
        });
}

function requestDcAccess(token, profile) {
    const endpoint = `${config.domains.api}/users/request-access`;
    return fetch(endpoint, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(profile),
    })
        .then(checkStatus)
        .then(parseJSON)
        .catch((error) => {
            // eslint-disable-next-line no-console
            console.log('request failed', error);
            throw error;
        });
}

export { requestDcAccess, reRequestDcAccess };

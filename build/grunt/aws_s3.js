module.exports = function replace(grunt, options) {
    return {
        options: {
            accessKeyId: process.env.AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL,
            secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY__CMC_DEMOCENTER_DL,
            region: 'us-west-2',
            uploadConcurrency: 5,
            downloadConcurrency: 5,
        },
        'dl-democenter': {
            options: {
                bucket: 'cmc-democenter',
                differential: true,
            },
            files: [
                { dest: '/', cwd: 'dist/product-center/', action: 'download' },
            ],
        },
    };
};

const path = require('path');
const timeGrunt = require('time-grunt');
const loadGruntConfig = require('load-grunt-config');
const loadDotEnv = require('cmc-load-dot-env');

if (!{}.hasOwnProperty.call(process.env, 'AWS_ACCESS_KEY_ID__CMC_DEMOCENTER_DL')) {
    loadDotEnv();
}

const modulesDir = path.resolve(require.resolve('cmc-site/Gruntfile')
    .replace('/Gruntfile.js', ''), '..');

const config = {
    app: 'app',
    dist: 'dist',
    sassOutput: '.tmp/sass-out/',
    modulesDir,
};

module.exports = function gruntfile(grunt) {
    timeGrunt(grunt);
    loadGruntConfig(grunt, {
        configPath: path.join(process.cwd(), '/build/grunt'),
        data: config,
        jitGrunt: {
            staticMappings: {
                scsslint: 'grunt-scss-lint',
                replace: 'grunt-text-replace',
                aws_s3: 'grunt-aws-s3',
            },
        },
    });

    grunt.registerTask('ssoIframe', [
        'copy:ssoIframe',
        'replace:ssoIframe',
        'run:ssoIframe',
    ]);

    grunt.registerTask('build', [
        'env:prod',
        'clean',
        'ssoIframe',
        'concurrent:dist',
    ]);

    grunt.registerTask('watchPrep', [
        'clean',
        'ssoIframe',
        'styles',
        'imagemin',
        'svgmin',
        'copy:dist',
        'run:metalsmithDist',
        'aws_s3:dl-democenter',
    ]);

    grunt.registerTask('justWatch', [
        'concurrent:justWatch',
    ]);

    grunt.registerTask('styles', [
        'sass',
        'autoprefixer',
        'cssmin',
        'concat',
    ]);

    grunt.registerTask('dist', ['build', 'notify:dist']);
};

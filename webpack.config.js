const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const production = process.env.NODE_ENV === 'production';
const appEntry = [path.join(__dirname, 'app/scripts/index.js')];

const modulesDir = path.resolve(
    require.resolve('cmc-site/Gruntfile').replace('/Gruntfile.js', ''), '..'
);

const partialDirs = path.resolve('.', modulesDir, 'cmc-site/app/metalsmith/partials');

const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
        'window.jQuery': 'jquery',
        jQuery: 'jquery',
        Promise: 'core-js/es6/promise',
        fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
    new webpack.optimize.CommonsChunkPlugin({
        name: 'cmc-demo-center',
        minChunks: 3,
    }),
];

if (production) {
    plugins.push(
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({
            mangle: true,
            output: {
                comments: false,
            },
            compress: {
                warnings: true,
                drop_debugger: false,
            },
        })
    );
} else {
    appEntry.unshift('webpack/hot/dev-server', 'webpack-hot-middleware/client');
    plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
}

module.exports = {
    debug: !production,
    // profile: true,
    devtool: production ? 'source-map' : '#eval-source-map',
    context: path.join(__dirname, 'app', 'scripts'),

    entry: {
        'cmc-demo-center': appEntry,
        'page--profile': path.join(__dirname, 'app/scripts/page--profile'),
        'page--change-password': path.join(__dirname, 'app/scripts/page--change-password'),
        'page--password-reset': path.join(__dirname, 'app/scripts/page--password-reset'),
        'page--register': path.join(__dirname, 'app/scripts/page--register'),
    },

    resolve: {
        root: [path.join(__dirname, 'app')],
        alias: {
            config: path.join(__dirname, 'config.js'),
        },
    },

    output: {
        path: path.join(__dirname, 'dist/'),
        // filename: production ? 'scripts/[name]-[hash].js' : 'scripts/[name].js',
        filename: 'scripts/[name].js',
        // chunkFilename: production ? 'scripts/[name]-[chunkhash].js' : 'scripts/[name].js',
        chunkFilename: 'scripts/[name].js',
        publicPath: '',
    },

    plugins,

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel'],
                include: [
                    path.resolve('.', 'app'),
                    path.resolve('.', 'config.js'),
                ],
            },
            {
                test: /\.js$/,
                loaders: ['babel'],
                include: [
                    fs.realpathSync(`${modulesDir}/cmc-site`),
                    fs.realpathSync(`${modulesDir}/cmc-auth`),
                ],
            },
            {
                test: /\.hbs$/,
                loaders: [
                    'monkey-hot',
                    `handlebars-loader?runtime=handlebars/runtime,partialDirs[]=${partialDirs}`,
                ],
                include: path.join(__dirname, '/app/'),
            },
            {
                test: /\.hbs$/,
                loaders: ['handlebars-loader?runtime=handlebars/runtime'],
                include: [
                    fs.realpathSync(`${modulesDir}/cmc-site`),
                    fs.realpathSync(`${modulesDir}/cmc-auth`),
                ],
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader',
                query: {
                    variable: 'data',
                },
            },
        ],
    },
};

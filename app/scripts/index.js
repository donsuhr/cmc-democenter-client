// eslint-disable-next-line no-unused-vars
import cmcSite from 'cmc-site/app/scripts/index';
import $ from 'jquery';
import { auth } from 'cmc-auth';
import config from 'config';
import { reRequestDcAccess } from '../components/request-dc-access';

auth.setup(config);
auth.init();

$('.demo-center-form-left__resend-request-button').on('click', (event) => {
    event.preventDefault();
    const $sending = $('<p>Sending...</p>');
    const $sent = $('<p>Sent</p>');
    $(event.target).off().replaceWith($sending);
    reRequestDcAccess(auth.getToken()).then(fetchResult => {
        $sending.replaceWith($sent);
        setTimeout(timeoutEvent => {
            $sent.remove();
        }, 2000);
    });
});

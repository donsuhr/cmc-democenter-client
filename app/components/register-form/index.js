import _ from 'lodash';
import $ from 'jquery';
import parsley from 'parsleyjs'; // eslint-disable-line no-unused-vars
import garlic from 'Garlic.js'; // eslint-disable-line no-unused-vars
import formSerialize from 'form-serialize';
import { profile } from 'cmc-auth';
import { requestDcAccess } from '../request-dc-access';

import template from './form.hbs';
import successTemplate from './success.hbs';

let _$loadingEl;

function clearGarlicLocalStorage() {
    _.each(_.keys(localStorage), key => {
        if (key.match(/^garlic/)) {
            localStorage.removeItem(key);
        }
    });
}

function hideLoading() {
    if (_$loadingEl && _$loadingEl.length) {
        _$loadingEl.remove();
    }
}

function showLoading($el) {
    hideLoading();
    _$loadingEl = $('<div class="loading">Loading...</div>');
    const x = ($el.innerWidth() / 2) - (_$loadingEl.innerWidth() / 2);
    const y = Math.max(0, (($el.innerHeight() / 2) - (_$loadingEl.innerHeight() / 2)));
    _$loadingEl.css({ left: x, top: y });
    $el.append(_$loadingEl);
}

function submitSighupForm(state) {
    showLoading(state.$el);
    const form = state.$el.find('.register-form').get(0);
    const userMetadata = formSerialize(form, { hash: true, empty: true, disabled: true });
    userMetadata.source = state.source;
    const user = userMetadata.username;
    const pass = userMetadata.password;
    delete userMetadata.username;
    delete userMetadata.password;
    delete userMetadata['password-repeat'];

    state.auth
        .signup(user, pass)
        .then(({ token }) => {
            clearGarlicLocalStorage();
            return profile.updateProfile(token, { user_metadata: userMetadata });
        })
        .then((profileData) => requestDcAccess(state.auth.getToken(), profileData))
        .then(() => {
            state.$el.html(successTemplate());
        })
        .catch((error) => {
            const message = error.code === 'user_exists' ?
                'An account with the supplied email exists' :
                'Error Requesting Access';
            state.$el.find('.register-form__error').text(message);
        })
        .then(() => {
            hideLoading();
        });
}

function register(state) {
    profile.setup(state.config);
    const html = template({ isRegister: true });
    state.$el.html(html);
    const $form = state.$el.find('.register-form');
    $form.garlic();
    const parsleyApi = $form.parsley();
    $form.on('click', '.register-form__submit-btn', event => {
        event.preventDefault();
        const valid = parsleyApi.validate();
        if (valid) {
            submitSighupForm(state);
        }
    });

    return {};
}

export default {
    create: function create(options) {
        const state = _.assign({}, options);
        return _.assign({}, register(state));
    },
};

module.exports = function babel(grunt, options) {
    return {
        options: {
            sourceMap: false,
        },
        dist: {
            files: [{
                expand: true,
                cwd: '.',
                src: [`${options.app}/scripts/**/*.js`],
                dest: '.tmp/babel-out/',
                ext: '.js',
                extDot: 'last',
            }],
        },
    };
};

import $ from 'jquery';
import config from 'config';
import { auth } from 'cmc-auth';
import profileDisplay from '../components/profile-display';

auth.setup(config);
auth.init();

profileDisplay.create({
    $el: $('.profile-form-container'),
    auth,
    config,
});


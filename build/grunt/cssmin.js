module.exports = function cssmin(grunt, options) {
    return {
        options: {
            sourceMap: true,
        },
        dist: {
            files: [
                {
                    expand: true,
                    cwd: '.tmp/styles',
                    src: ['**/*.css'],
                    dest: '.tmp/styles/',
                    ext: '.css',
                },
            ],
        },
    };
};

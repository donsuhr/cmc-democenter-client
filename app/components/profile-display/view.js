import _ from 'lodash';
import $ from 'jquery';
import templateDisplay from './display.hbs';
import templateForm from './form-wrapper.hbs';

require('parsleyjs');
require('Garlic.js');

function profileView(state) {
    const _model = state.profile;
    const _authApi = state.auth;
    const _$el = state.$el;
    let _$loadingEl;

    return {
        render(mode) {
            if (mode === 'display') {
                $('.cmc-article-header__title').text('Profile');
                $('.cmc-article-header__bg-image')
                    .attr('src', '/images/banners/Banner_Profile.png');
            } else {
                $('.cmc-article-header__title').text('Edit Profile');
                $('.cmc-article-header__bg-image').attr('src', '/images/banners/Banner_Edit.png');
            }
            this.showLoading();
            const currentForm = _$el.find('form');
            if (currentForm.length) {
                $(currentForm).parsley().destroy();
            }
            const template = mode === 'display' ? templateDisplay : templateForm;
            _model.getProfile(_authApi)
                .then((profileData) => {
                    _$el.html(template({ profileData }));
                    const $newForm = _$el.find('form');
                    if ($newForm.length) {
                        $newForm.parsley();
                        $newForm.garlic();
                        $newForm
                            .find('[name="country"] option')
                            .filter(
                                function countryFilter() {
                                    return $(this).text() === profileData.user_metadata.country;
                                }
                            )
                            .prop('selected', true);
                    }
                })
                .catch((error) => {
                    _$el.html(template({ error }));
                });
        },
        showError(error) {
            _$el.find('.profile-form__error')
                .text(`Error updating profile. Please try again later. (${error.message})`);
        },
        showLoading() {
            this.hideLoading();
            _$loadingEl = $('<div class="loading">Loading...</div>');
            const x = (_$el.innerWidth() / 2) - (_$loadingEl.innerWidth() / 2);
            const y = Math.max(0, ((_$el.innerHeight() / 2) - (_$loadingEl.innerHeight() / 2)));
            _$loadingEl.css({ left: x, top: y });
            _$el.append(_$loadingEl);
        },
        hideLoading() {
            if (_$loadingEl && _$loadingEl.length) {
                _$loadingEl.remove();
            }
        },
    };
}

export default {
    create(options) {
        return _.assign({}, profileView(options));
    },
};

import $ from 'jquery';
require('parsleyjs');

let _$loadingEl;

function showError(error, $el) {
    $el.find('.change-password-form__error')
        .text(error);
}

function hideLoading() {
    if (_$loadingEl && _$loadingEl.length) {
        _$loadingEl.remove();
    }
}

function showLoading($el) {
    hideLoading();
    _$loadingEl = $('<div class="loading">Loading...</div>');
    const x = ($el.innerWidth() / 2) - (_$loadingEl.innerWidth() / 2);
    const y = Math.max(0, (($el.innerHeight() / 2) - (_$loadingEl.innerHeight() / 2)));
    _$loadingEl.css({ left: x, top: y });
    $el.append(_$loadingEl);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}

function parseJSON(response) {
    return response.json();
}

function getEmail(options) {
    if (options.mode === 'change') {
        return options.profile.getProfile().then(profileData => profileData.email);
    } else {
        const emailVal = $.trim(options.$el.find('[name="email"]').val());
        return fetch(`https://api.suhrthing.com:9000/users/by-email/${emailVal}`)
            .then(checkStatus)
            .then(parseJSON)
            .then(result => {
                if (result.total === 0) {
                    throw new Error('Email Not Found');
                }
                return emailVal;
            });
    }
}

function addListeners(options) {
    options.$el
        .on('click', '.change-password-form__cancel-btn', (event) => {
            event.preventDefault();
            showLoading(options.$el);
            window.history.back();
        })
        .on('click', '.change-password-form__submit-btn', (event) => {
            event.preventDefault();
            showLoading(options.$el);
            let valid = true;
            if (options.mode === 'reset') {
                const parsleyApi = options.$el.find('.cmc-form--change-password').parsley();
                valid = parsleyApi.validate();
            }
            if (valid) {
                getEmail(options)
                    .then(email => {
                        options.auth.updatePassword(email)
                            .then(result => {
                                showError(result, options.$el);
                                options.$el.find('[name="email"]').prop('disabled', true);
                                $('.change-password-form__cancel-btn').text('Back');
                                $('.change-password-form__submit-btn').css('display', 'none');
                            })
                            .catch(error => {
                                console.log('error', error); // eslint-disable-line no-console
                                showError(
                                    `Error updating password. (${error.message})`, options.$el
                                );
                            });
                    })
                    .catch(error => {
                        showError('Email address not found', options.$el);
                    })
                    .then(() => {
                        hideLoading();
                    });
            } else {
                hideLoading();
            }
        });
}

const api = {
    init(options) {
        showLoading(options.$el);
        if (options.mode === 'change') {
            const auth = options.auth;
            if (!auth.isAuthenticated()) {
                window.location.href = '/';
            }
        }
        addListeners(options);
        hideLoading();
    },
};

export default api;

/*
 |--------------------------------------------------------------------------
 | Browser-sync config file
 |--------------------------------------------------------------------------
 |
 | For up-to-date information about the options:
 |   http://www.browsersync.io/docs/options/
 |
 | There are more options than you see here, these are just the ones that are
 | set internally. See the website for more info.
 |
 |
 */
const webpack = require('webpack');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpackDevMiddleware = require('webpack-dev-middleware');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config');
const bundler = webpack(webpackConfig);

module.exports = {
    https: {
        // force enable /node_modules/browser-sync/lib/server/utils.js
        /**
         getKeyAndCert: function (options) {
                return {
                    // key: fs.readFileSync(
                    //    options.getIn(['https', 'key']) ||
                    //    filePath.join(__dirname, 'certs/server.key')
                    // ),
                    // cert: fs.readFileSync(
                    //    options.getIn(['https', 'cert']) ||
                    //    filePath.join(__dirname, 'certs/server.crt')
                    // ),
                    key: fs.readFileSync(
                        '/private/etc/letsencrypt/live/api.suhrthing.com/privkey.pem'
                    ),
                    cert: fs.readFileSync(
                        '/private/etc/letsencrypt/live/api.suhrthing.com/cert.pem'
                    ),
                };
            },
         **/
        key: '/private/etc/letsencrypt/live/api.suhrthing.com/privkey.pem',
        cert: '/private/etc/letsencrypt/live/api.suhrthing.com/cert.pem',
    },
    files: [
        'dist/**/*.*',
    ],
    proxy: {
        target: 'https://product-center.suhrthing.com:9003',
    },
    port: 9002,
    middleware: [
        webpackDevMiddleware(bundler, {
            // IMPORTANT: dev middleware can't access config, so we should
            // provide publicPath by ourselves
            publicPath: webpackConfig.output.publicPath,
            stats: {
                colors: true,
            },
        }),
        webpackHotMiddleware(bundler),
        (req, res, next) => {
            if (req.method === 'POST') {
                req.method = 'GET';
            }
            return next();
        },
    ],
    open: false,
    notify: true,
    scrollThrottle: 100,
    reloadDelay: 500,
    reloadDebounce: 500,
    injectChanges: true,
    ui: {
        port: 35732,
        weinre: {
            port: 8082,
        },
    },
};

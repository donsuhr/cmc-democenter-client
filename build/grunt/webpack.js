const webpackConfig = require('../../webpack.config.js');

module.exports = function wp(grunt, options) {
    return {
        options: webpackConfig,
        dist: {
            progress: false,
            watch: false,
            keepalive: false,
            inline: false,
            hot: false,
            cache: false,
            stats: {
                colors: true,
                modules: false,
                reasons: false,
                errorDetails: true,
            },
        },
    };
};

module.exports = function run(grunt, options) {
    return {
        metalsmith: {
            options: {
                wait: true,
            },
            exec: 'npm run watch:html',
        },
        metalsmithDist: {
            exec: 'npm run build:html',
        },
        ssoIframe: {
            exec: 'npm run build:js:ssoIframe',
        },
    };
};

module.exports = function watch(grunt, options) {
    return {
        js: {
            files: [
                './**/*.js',
                '!./app/scripts/vendor/**',
                '!./node_modules/**',
                '!./dist/**',
                '!./.tmp/**',
            ],
            tasks: ['eslint'],
        },
        sass: {
            files: ['./app/**/*.scss'],
            tasks: [
                'styles',
                'scsslint',
            ],
        },
    };
};


module.exports = function concurrent(grunt, options) {
    return {
        options: {
            logConcurrentOutput: true,
            limit: 8,
        },
        dist: [
            'styles',
            'imagemin',
            'svgmin',
            'copy:dist',
            'webpack:dist',
            'run:metalsmithDist',
            'aws_s3:dl-democenter',
        ],
        justWatch: [
            'watch:sass',
            'watch:js',
            'run:metalsmith',
        ],
    };
};

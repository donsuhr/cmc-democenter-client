module.exports = function eslint(grunt, options) {
    return {
        options: {
            cache: true,
        },
        lint: {
            src: [
                './**/*.js',
                '!./app/scripts/vendor/**',
                '!./.tmp/**',
                '!./dist/**',
                '!./node_modules/**',
            ],
        },
    };
};

const path = require('path');

module.exports = function copy(grunt, options) {
    return {
        dist: {
            options: {
                sourceMap: true,
                sourceMapName: `${options.dist}/styles/main.css.map`,
                sourceMapStyle: 'link',
            },
            files: [
                {
                    src: [
                        path.resolve(options.modulesDir, 'cmc-site/peer-assets/styles/main.css'),
                        '.tmp/styles/cmc-democenter.css',
                    ],
                    dest: `${options.dist}/styles/main.css`,
                },
            ],
        },
    };
};

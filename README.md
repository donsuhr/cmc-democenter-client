# cmc-democenter-client

metalsmith and js processors for the demo center, now Product Center

```
# when running under cmc-democenter-server proxy
npm run bs
```


## update the ssoIframe
```
grunt ssoIframe
```

## dist
```
npm run dist
```

## update the cmc packages
```
./scripts/update-npm-cmc-peer.sh
```

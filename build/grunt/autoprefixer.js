module.exports = function autoprefixer(grunt, options) {
    return {
        options: {
            browsers: ['> 5%', 'last 3 versions'],
            map: {
                inline: false,
            },
        },
        dist: {
            files: [
                {
                    expand: true,
                    cwd: options.sassOutput, // '<%= compass.options.cssDir %>',
                    src: '**/*.css',
                    dest: '.tmp/styles/',
                },
            ],
        },
    };
};

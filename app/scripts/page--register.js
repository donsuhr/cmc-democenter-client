import $ from 'jquery';
import register from 'components/register-form';
import config from 'config';
import { auth } from 'cmc-auth';

auth.setup(config);
auth.init();

const $el = $('.register-form-container');
register.create({
    $el,
    auth,
    config,
    source: $el.data('source'),
});

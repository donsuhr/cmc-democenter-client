const path = require('path');
const fs = require('fs');
const argv = require('yargs').argv;
const chokidar = argv.watch ? require('chokidar') : null;
const glob = require('glob');

const Metalsmith = require('metalsmith');
const layouts = require('metalsmith-layouts');
const inPlace = require('metalsmith-in-place');
const helpers = require('metalsmith-register-helpers');
const permalinks = require('metalsmith-permalinks');
const defaultValues = require('metalsmith-default-values');
const partials = require('metalsmith-register-partials');
const addOriginalFilename = require('metalsmith-add-orig-file-name');
const handlebars = require('handlebars');
const handlebarsLayouts = require('handlebars-layouts');

// assemble helpers have the handlebars-layouts in it. assemble first
require('handlebars-helpers')({
    handlebars,
}); // http://assemble.io/helpers/
// overwrite second
handlebars.registerHelper(handlebarsLayouts(handlebars));

require('babel-register')({
    only: /config.js/,
});

const modulesDir = path.resolve(require.resolve('cmc-site/Gruntfile')
    .replace('/Gruntfile.js', ''), '..');

const config = require('../config');

function replaceScriptSrc() {
    glob('./dist/**/*.html', (er, files) => {
        files.forEach(file => {
            const htmlpath = path.resolve(file);
            fs.readFile(htmlpath, 'utf8', (rfErr, data) => {
                if (rfErr) {
                    throw rfErr;
                }
                const result = data.replace(
                    /\/scripts\/cmc-site\.js/gi,
                    '/scripts/cmc-demo-center.js'
                );
                fs.writeFile(htmlpath, result, 'utf8', (wfErr) => {
                    if (wfErr) {
                        throw wfErr;
                    }
                });
            });
        });
    });
}

function build(clean) {
    const ms = new Metalsmith(path.resolve(`${__dirname}/../`));
    ms.clean(!!clean)
        .source('app/html/pages')
        .destination('dist')
        .metadata({
            config,
        })
        .use(
            addOriginalFilename({})
        )
        .use(
            defaultValues([
                {
                    pattern: '**/*.html',
                    defaults: {
                        priority: 0.5,
                        'use-generated-partials': true,
                        'show-in-left-nav': true,
                        'start-with-menu-collapsed': true,
                        'scroll-menu-with-page': true,
                    },
                },
            ])
        )
        .use(
            permalinks({
                relative: false,
            })
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: 'app/html/helpers',
            })
        )
        .use(
            helpers({
                engine: 'handlebars',
                directory: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/helpers')
                ),
            })
        )
        .use(
            partials({
                // pre made menu
                directory: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/peer-assets/partials')
                ),
                handlebars,
            })
        )
        .use(
            partials({
                directory: 'app/html/partials',
                handlebars,
            })
        )
        .use(
            partials({
                directory: 'app/html/layouts',
                handlebars,
            })
        )
        .use(
            inPlace({
                engine: 'handlebars',
                partials: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/partials')
                ),
            })
        )
        .use(
            layouts({
                // files using cmc-site layouts folder
                engine: 'handlebars',
                directory: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/layouts')
                ),
                partials: path.relative(
                    '.', path.resolve(modulesDir, 'cmc-site/app/metalsmith/partials')
                ),
                pattern: ['**/*'],
            })
        )
        .build(err => {
            if (err) {
                throw err;
            }
            replaceScriptSrc();
        });
}

if (argv.watch) {
    chokidar.watch([
        'app/html/**/*.html',
        'app/html/layouts/**/*.hbs',
        'app/html/partials/**/*.hbs',
        'app/html/helpers/**/*.js',
    ]).on('all', (event, filePath) => {
        // eslint-disable-next-line no-console
        console.log('chokidar watch: ', event, filePath);
        build(false);
    });
}

build(!!argv.clean);

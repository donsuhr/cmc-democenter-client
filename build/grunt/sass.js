module.exports = function sass(grunt, options) {
    return {
        options: {
            sourceMap: true,
            includePaths: [options.modulesDir],
        },
        dev: {
            files: [{
                expand: true,
                cwd: 'app/styles',
                src: ['**/*.scss'],
                dest: options.sassOutput,
                ext: '.css',
            }],
        },
    };
};

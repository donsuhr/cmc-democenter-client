import $ from 'jquery';
import config from 'config';
import { auth, profile } from 'cmc-auth';
import changePassword from '../components/change-password';

auth.setup(config);
auth.init();
profile.setup(config);

changePassword.init({
    $el: $('.change-password-form-container'),
    auth,
    profile,
    config,
    mode: 'change',
});

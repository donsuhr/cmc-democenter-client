const path = require('path');

module.exports = function copy(grunt, options) {
    return {
        dist: {
            files: [
                {
                    // non imagemin / svgmin
                    expand: true,
                    dot: true,
                    cwd: options.app,
                    dest: options.dist,
                    src: [
                        '*.{ico,png,txt}',
                        'images/{,*/}*.webp',
                        'fonts/{,*/}*.*',
                    ],
                },
                {
                    expand: true,
                    dot: true,
                    cwd: path.resolve(options.modulesDir, 'cmc-site/peer-assets'),
                    src: ['images/**/*', 'styles/**/*'],
                    dest: `${options.dist}`,
                },
            ],
        },
        ssoIframe: {
            files: [
                {
                    expand: true,
                    dot: true,
                    cwd: path.resolve(options.modulesDir, 'cmc-site'),
                    dest: '.tmp',
                    src: [
                        'webpack.config-ssoIframe.js',
                    ],
                },
                {
                    expand: true,
                    dot: true,
                    cwd: path.resolve(options.modulesDir, 'cmc-site/app/scripts'),
                    dest: '.tmp/scripts',
                    src: [
                        'sso-iframe.js',
                    ],
                },
            ],
        },
    };
};
